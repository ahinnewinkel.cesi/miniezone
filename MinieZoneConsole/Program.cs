﻿using System;
using MinieZoneLibrary;

namespace MinieZoneConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();

            Address deliveryaddress = new Address("5", "cour Kratz", "67000", "Strasbourg", "Allemagne");
            InvoiceAddress invoiceaddress = new InvoiceAddress("Hinnewinkel", "Alexis", "5", "cour Kratz", "67000", "Strasbourg", "France");
            Commande commande = new Commande(deliveryaddress, invoiceaddress);
            try
            {
                Article article1 = new Article("Voiture", (random.Next(-10000, 100000) / 100), ArticlesManager.TAUX_NORMAL);
                commande.Articles.Add(new CommandeArticle(article1, random.Next(1, 10)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Article article2 = new Article("Switch", (random.Next(-1000, 100000) / 100), ArticlesManager.TAUX_NORMAL);
                commande.Articles.Add(new CommandeArticle(article2, random.Next(1, 10)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Article article3 = new Article("Pomme", (random.Next(-1000, 100000) / 100), ArticlesManager.TAUX_REDUIT);
                commande.Articles.Add(new CommandeArticle(article3, random.Next(1, 10)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine(commande.GetDetails());

            // Les doublons sont optimisés pour des calculs rapides mais peu précis
            // Les décimaux font des opération plus longues mais les calculs sont très précis
            decimal number = 0.1M;
            double number2 = 0.1;
            Console.WriteLine(number + number + number + number + number + number + number + number + number + number);
            Console.WriteLine(number * 10);
            Console.WriteLine(number2 + number2 + number2 + number2 + number2 + number2 + number2 + number2 + number2 + number2);
            Console.WriteLine(number2 * 10);
        }
    }
}

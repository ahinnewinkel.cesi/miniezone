﻿namespace MinieZoneLibrary
{
    public class Address
    {
        public string Informations { get; set; }
        public string Number { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public Address(string number, string street, string zipcode, string city, string country, string informations)
        {
            this.Number = number;
            this.Street = street;
            this.ZipCode = zipcode;
            this.City = city;
            this.Country = country;
            this.Informations = informations;
        }

        public Address(string number, string street, string zipcode, string city, string country)
            : this(number, street, zipcode, city, country, "")
        {
        }

        public override string ToString()
        {
            string address = this.Number + " " + this.Street + "\n";
            if (!this.Informations.Equals(""))
            {
                address += this.Informations + "\n";
            }
            address += this.ZipCode + " " + this.City + "\n" + this.Country;

            return address;
        }
    }
}
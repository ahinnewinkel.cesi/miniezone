﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    interface IId
    {
        int GetNextId();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    public class ArticlesManager : Manager
    {
        public const decimal TAUX_NORMAL = 0.2M;
        public const decimal TAUX_INTERMEDIAIRE = 0.1M;
        public const decimal TAUX_REDUIT = 0.055M;
        public const decimal TAUX_PATICULIER = 0.021M;
        public ArticlesManager()
            : base()
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    public abstract class Manager : IId
    {
        protected int NextId;

        public Manager()
        {
            this.NextId = 1;
        }

        public int GetNextId()
        {
            // Renvoie la valeur courante puis fait le ++
            return this.NextId++;
        }
    }
}

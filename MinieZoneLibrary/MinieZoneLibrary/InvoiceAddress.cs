﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    public class InvoiceAddress : Address
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public InvoiceAddress(string lastname, string firstname, string number, string street, string zipcode, string city, string country)
            : this(lastname, firstname, number, street, zipcode, city, country, "")
        {

        }
        public InvoiceAddress(string lastname, string firstname, string number, string street, string zipcode, string city, string country, string informations)
            : base(number, street, zipcode, city, country, informations)
        {
            this.LastName = lastname;
            this.FirstName = firstname;
        }

        public override string ToString()
        {
            return this.FirstName + " " + this.LastName + "\n" + base.ToString();
        }
    }
}

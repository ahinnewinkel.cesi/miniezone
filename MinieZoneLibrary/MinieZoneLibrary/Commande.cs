﻿using System;
using System.Collections.Generic;

namespace MinieZoneLibrary
{
    // https://docs.microsoft.com/fr-fr/dotnet/csharp/programming-guide/classes-and-structs/partial-classes-and-methods
    // https://docs.microsoft.com/fr-fr/dotnet/csharp/language-reference/keywords/struct
    public partial class Commande
    {
        // écrire prop puis 2x tab pour générer automatiquement des éléments
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public List<CommandeArticle> Articles { get; set; }
        public Address DeliveryAddress { get; set; }
        public InvoiceAddress InvoiceAddress { get; set; }

        public Commande(Address deliveryaddress, InvoiceAddress invoiceaddress)
            : this((new CommandesManager()).GetNextId(), DateTime.Now, deliveryaddress, invoiceaddress)
        {

        }

        public Commande(int id, DateTime date, Address deliveryaddress, InvoiceAddress invoiceaddress)
        {
            this.Id = id;
            this.Date = date;
            this.Articles = new List<CommandeArticle>();
            this.DeliveryAddress = deliveryaddress;
            this.InvoiceAddress = invoiceaddress;
        }
    }

    public partial class Commande
    {
        public string GetDetails()
        {
            string details = "Commande n°" + this.Id + "\n";
            details += "Liste des articles\n";
            this.Articles.ForEach(commandearticle => details += commandearticle.Article.Name + " x" + commandearticle.Quantity + " : " + commandearticle.Article.Price + "\n");
            details += "Montant HT : " + String.Format("{0:0.00}", this.GetExclTaxesAmountTotal()) + "\n";
            details += "Montant TVA : " + String.Format("{0:0.00}", this.GetTaxesAmountTotal()) + "\n";
            details += "Montant TTC : " + String.Format("{0:0.00}", this.GetInclTaxesAmountTotal()) + "\n";
            details += "Adresse de livraison :\n";
            details += this.DeliveryAddress + "\n";
            details += "Adresse de facturation :\n";
            details += this.InvoiceAddress + "\n";
            details += "Frais de port :\n";
            details += String.Format("{0:0.00}", this.GetDeliveryCost()) + "\n";
            details += "Prix moyen d'un article :\n";
            details += String.Format("{0:0.00}", this.GetAvgExclTaxesAmount()) + "\n";
            return details;
        }

        // Double => opération mathématiques peu rapide
        // Decimal => très précis
        public decimal GetExclTaxesAmountTotal()
        {
            decimal total = 0;
            Articles.ForEach(commandearticle => total += commandearticle.GetExclTaxesAmountSum());
            return total;
        }

        public decimal GetTaxesAmountTotal()
        {
            decimal total = 0;
            Articles.ForEach(commandearticle => total += commandearticle.GetTaxesAmountSum());
            return total;
        }

        public decimal GetInclTaxesAmountTotal()
        {
            decimal total = 0;
            Articles.ForEach(commandearticle => total += commandearticle.GetInclTaxesAmountSum());
            return total;
        }

        public int GetQuantitiesTotal()
        {
            int total = 0;
            Articles.ForEach(commandearticle => total += commandearticle.Quantity);
            return total;
        }

        public decimal GetAvgExclTaxesAmount()
        {
            return this.GetExclTaxesAmountTotal() / this.GetQuantitiesTotal();
        }

        public int GetDeliveryCost()
        {
            if (Articles.Count > 3)
            {
                return 0;
            }
            else
            {
                string[] europe = { "Allemagne", "Autriche", "Belgique", "Bulgarie", "Chypre", "Croatie", "Danemark", "Espagne", "Estonie", "Finlande", "France", "Grèce", "Hongrie", "Irlande", "Italie", "Lituanie", "Lettonie", "Luxembourg", "Malte", "Pays-Bas", "Pologne", "Portugal", "République Tchèque", "Roumanie", "Royaume-Uni", "Slovaquie", "Slovénie", "Suède" };
                if (this.DeliveryAddress.Country.Equals("France"))
                {
                    return 5;
                }
                else if (Array.Exists(europe, element => element.Equals(this.DeliveryAddress.Country)))
                {
                    return 10;
                }
                else
                {
                    return 20;
                }
            }
        }
    }
}

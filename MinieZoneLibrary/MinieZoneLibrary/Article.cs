﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    public class Article
    {
        public string Name { get; set; }
        private decimal _price;
        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                /*if (value < 0)
                {
                    _price = Math.Abs(value);
                }*/
                if (value < 0)
                {
                    throw new Exception("Price can't be lower than 0");
                }

                _price = value;
            }
        }
        public decimal Tax { get; }

        public Article(string name, decimal price, decimal tax)
        {
            this.Name = name;
            this.Price = price;
            this.Tax = tax;
        }

        // Taxes amount including VAT ([USA] or sales tax [GB])
        public decimal GetTaxesAmount()
        {
            return this.Price * this.Tax;
        }

        // All taxes included amount
        public decimal GetInclTaxesAmount()
        {
            return this.Price + this.GetTaxesAmount();
        }
    }
}

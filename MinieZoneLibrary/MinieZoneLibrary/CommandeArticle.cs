﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MinieZoneLibrary
{
    public class CommandeArticle
    {
        public int Quantity { get; set; }
        public Article Article { get; }

        public CommandeArticle(Article article, int quantity)
        {
            this.Article = article;
            this.Quantity = quantity;
        }

        public decimal GetExclTaxesAmountSum()
        {
            return this.Article.Price * this.Quantity;
        }

        public decimal GetTaxesAmountSum()
        {
            return this.Article.GetTaxesAmount() * this.Quantity;
        }

        public decimal GetInclTaxesAmountSum()
        {
            return this.Article.GetInclTaxesAmount() * this.Quantity;
        }
    }
}
